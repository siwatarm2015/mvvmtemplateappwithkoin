package com.example.mvvmappliacationtemplate.di

import com.example.mvvmappliacationtemplate.service.ApiService
import com.example.mvvmappliacationtemplate.service.RetrofitConnection
import org.koin.core.qualifier.named
import org.koin.dsl.module

    val networkModule = module {
        single { RetrofitConnection() }
        single { ApiService() }
//        scope(named<RetrofitConnection>()){
//            scoped { ApiService() }
//        }
    }
