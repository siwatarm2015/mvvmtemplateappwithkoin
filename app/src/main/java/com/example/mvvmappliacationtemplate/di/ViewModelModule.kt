package com.example.mvvmappliacationtemplate.di

import com.example.mvvmappliacationtemplate.repository.UserRepository
import com.example.mvvmappliacationtemplate.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get())}
    single { UserRepository(get())}
}