package com.example.mvvmappliacationtemplate.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mvvmappliacationtemplate.R

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }

}
