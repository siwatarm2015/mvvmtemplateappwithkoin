package com.example.mvvmappliacationtemplate.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmappliacationtemplate.model.User
import com.example.mvvmappliacationtemplate.repository.UserRepository
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject

class MainViewModel(private val userRepository: UserRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val setTodoSubject: PublishSubject<User> = PublishSubject.create()
    private val toDoList = MutableLiveData<User>()

    fun getTodo() {
        userRepository.loadUser(1).subscribe({ response ->
            when (response.code()) {
                200 -> {
                    toDoList.value = response.body()
                    toDoList.value?.let {
                        setTodoSubject.onNext(it)
                    }
                }
            }
        }, {
            it.printStackTrace()
        }).addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}

