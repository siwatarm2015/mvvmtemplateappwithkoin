package com.example.mvvmappliacationtemplate.model

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("userId")
    val userId: String? = null,
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("completed")
    val completed: String? = null
)