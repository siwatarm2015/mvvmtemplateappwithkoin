package com.example.mvvmappliacationtemplate.repository

import com.example.mvvmappliacationtemplate.model.User
import com.example.mvvmappliacationtemplate.service.ApiService
import com.example.mvvmappliacationtemplate.testing.OpenForTesting
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

/**
 * Repository that handles User objects.
 */
@OpenForTesting
class UserRepository(private val apiService: ApiService) {

    fun loadUser(id: Int): Observable<Response<User>> {
        return apiService.userService().getUser(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
    }

}