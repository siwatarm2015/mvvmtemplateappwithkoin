package com.example.mvvmappliacationtemplate.service

import com.example.mvvmappliacationtemplate.model.User
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * REST API access points
 */

interface UserService {

    @GET("todos/{id}")
    fun getUser(@Path("id") id: Int): Observable<Response<User>>
}